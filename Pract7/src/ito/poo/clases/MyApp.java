package ito.poo.clases;

import java.time.LocalDate;
import ito.poo.clases.MaquinaLavado;
import ito.poo.clases.MaquinaLlenadoyEnvasado;
import ito.poo.clases.MaquinaEmpaquetado;

public class MyApp {
	static void run() {
		
		MaquinaLavado lav= new MaquinaLavado("Maquina de Lavado", LocalDate.of(2020, 6, 24), 155, 10f, 30);
		MaquinaLlenadoyEnvasado llen= new MaquinaLlenadoyEnvasado("Maquina de LLenado", LocalDate.of(2021, 7, 11), 5000, 16, 100);
		MaquinaEmpaquetado empaq= new MaquinaEmpaquetado("Maquina de Empaquetado", LocalDate.of(2021, 9, 04), 361, 9, 3);
		
		System.out.println(lav);
		System.out.println("Costo de lavado por botella: " + lav.costoLavado() + " $ ");
		System.out.println(llen);
		System.out.println("Costo de llenado por botella:" + llen.costoLlenadoyEnvasado() + " $ ");
		System.out.println(empaq);
		System.out.println("Costo de empaquetado por botella:" + empaq.costoEmpaquetado() + " $ ");
	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
