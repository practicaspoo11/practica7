package ito.poo.clases;
import java.time.LocalDate;
public class MaquinaLlenadoyEnvasado extends Maquinas{
	private int CantidadEnvaces;
	private int Regulacion;
	
	public MaquinaLlenadoyEnvasado(String descripcion, LocalDate fechaAdquisicion, int costo, int cantidadEnvaces,
			int regulacion) {
		super(descripcion, fechaAdquisicion, costo);
		CantidadEnvaces = cantidadEnvaces;
		Regulacion = regulacion;
	}
	
	public float costoLlenadoyEnvasado() {
		return ((.25f * this.getCosto()) / 100) / this.CantidadEnvaces;
	}
	
	public int getCantidadEnvaces() {
		return CantidadEnvaces;
	}
	public void setCantidadEnvaces(int cantidadEnvaces) {
		CantidadEnvaces = cantidadEnvaces;
	}
	public int getRegulacion() {
		return Regulacion;
	}
	public void setRegulacion(int regulacion) {
		Regulacion = regulacion;
	}

	@Override
	public String toString() {
		return "MaquinaLlenadoyEnvasado [CantidadEnvaces=" + CantidadEnvaces + ", Regulacion=" + Regulacion + "]";
	}
	
	
}
