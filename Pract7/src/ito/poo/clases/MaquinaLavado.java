package ito.poo.clases;

import java.time.LocalDate;

public class MaquinaLavado extends Maquinas {
	
	private float CapacidadLitros;
	private int Tiempo;
	public MaquinaLavado(String descripcion, LocalDate fechaAdquisicion, int costo, float capacidadLitros, int tiempo) {
		super(descripcion, fechaAdquisicion, costo);
		CapacidadLitros = capacidadLitros;
		Tiempo = tiempo;
	}
	
	public float costoLavado() {
		return ((.05f * this.getCosto()) / 100) / (60/this.Tiempo);
	}
	public float getCapacidadLitros() {
		return CapacidadLitros;
	}
	public void setCapacidadLitros(float capacidadLitros) {
		CapacidadLitros = capacidadLitros;
	}
	public int getTiempo() {
		return Tiempo;
	}
	public void setTiempo(int tiempo) {
		Tiempo = tiempo;
	}

	@Override
	public String toString() {
		return "MaquinaLavado [CapacidadLitros=" + CapacidadLitros + ", Tiempo=" + Tiempo + "]";
	}

	
}
