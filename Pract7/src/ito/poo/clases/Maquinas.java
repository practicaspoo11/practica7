package ito.poo.clases;
import java.time.LocalDate;
public class Maquinas {
	
	private String Descripcion;
	private LocalDate FechaAdquisicion;
	private int costo;
	public Maquinas(String descripcion, LocalDate fechaAdquisicion, int costo) {
		super();
		Descripcion = descripcion;
		FechaAdquisicion = fechaAdquisicion;
		this.costo = costo;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public LocalDate getFechaAdquisicion() {
		return FechaAdquisicion;
	}
	public void setFechaAdquisicion(LocalDate fechaAdquisicion) {
		FechaAdquisicion = fechaAdquisicion;
	}
	public int getCosto() {
		return costo;
	}
	public void setCosto(int costo) {
		this.costo = costo;
	}
	@Override
	public String toString() {
		return "Maquinas [Descripcion=" + Descripcion + ", FechaAdquisicion=" + FechaAdquisicion + ", costo=" + costo
				+ "]";
	}
	


}
