package ito.poo.clases;

import java.time.LocalDate;

public class MaquinaEmpaquetado extends Maquinas{
	
	private int TipoEmpaque;
	private int CantidadEmpaques;
	
	public MaquinaEmpaquetado(String descripcion, LocalDate fechaAdquisicion, int costo, int tipoEmpaque,
			int cantidadEmpaques) {
		super(descripcion, fechaAdquisicion, costo);
		TipoEmpaque = tipoEmpaque;
		CantidadEmpaques = cantidadEmpaques;
	}
	public float costoEmpaquetado() {
		return ((.06f * this.getCosto()) / 100) / (this.CantidadEmpaques*this.TipoEmpaque);
	}
	public int getTipoEmpaque() {
		return TipoEmpaque;
	}
	public void setTipoEmpaque(int tipoEmpaque) {
		TipoEmpaque = tipoEmpaque;
	}
	public int getCantidadEmpaques() {
		return CantidadEmpaques;
	}
	public void setCantidadEmpaques(int cantidadEmpaques) {
		CantidadEmpaques = cantidadEmpaques;
	}
	@Override
	public String toString() {
		return "MaquinaEmpaquetado [TipoEmpaque=" + TipoEmpaque + ", CantidadEmpaques=" + CantidadEmpaques + "]";
	}
	
}
